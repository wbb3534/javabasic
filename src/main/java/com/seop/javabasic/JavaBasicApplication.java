package com.seop.javabasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasicApplication.class, args);

        boolean myBox1 = true;
        System.out.println(myBox1);

        char myBox2 = 'ㄹ';
        Character myBox222 = 'd';
        String myBox22 = "d";

        byte myBox3 = 1;
        Byte myBox33 = 1;

        short myBox4 = 3;
        int myBox5 = 4;
        long myBox6 = 11L;

        float myBox7 = 11.1f;
        double myBox8 = 1.2;
    }

}
